package com.ub.nextcargo.domen;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class CargoTest {
	@Test
public void CurgoConstructorTest() {
	Location origin = new Location();
	origin.id = "1";
	origin.name = "Метро Таганская";
	Location destination = new Location();
	destination.id = "1";
	destination.name = "метро Курская";
	LocalDate originDate = LocalDate.of(30, 8, 2019);
	LocalDate destinationDate = LocalDate.of(5, 9, 2019);
	double weight = 3;
	
	Cargo cargo = new Cargo(origin, destination, originDate, destinationDate, weight);
	
	assertEquals(origin, cargo.origin);
	assertEquals(destination, cargo.destination);
	assertEquals(originDate, cargo.originDate);
	assertEquals(destinationDate, cargo.destinationDate);
	assert(weight == cargo.weight);
	
};

@Test
 public void NewRouteTest() {
	 	Location origin = new Location();
		origin.id = "1";
		origin.name = "Метро Таганская";
		Location destination = new Location();
		destination.id = "1";
		destination.name = "метро Курская";
		LocalDate originDate = LocalDate.of(30, 8, 2019);
		LocalDate destinationDate = LocalDate.of(5, 9, 2019);
		double weight = 3;
		
		Cargo cargo = new Cargo(origin, destination, originDate, destinationDate, weight);
		
		Location origin1 = new Location();
		origin1.id = "2";
		origin1.name = "Метро Пролетарская";
		Location destination1 = new Location();
		destination1.id = "2";
		destination1.name = "метро Маяковская";
		LocalDate originDate1 = LocalDate.of(31, 12, 2019);
		LocalDate destinationDate1 = LocalDate.of(1, 1, 2020);
		
		cargo.NewRoute(origin1, destination1, originDate1, destinationDate1);
 }; 
 
 @Test
 public void NewItineraryTest() {
	 Itinerary itinerary = new Itinerary();
	 Enum routingStatus;
	 Enum transportationStatus;
 };


}
