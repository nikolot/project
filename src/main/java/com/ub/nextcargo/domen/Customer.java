package com.ub.nextcargo;

import java.time.LocalDate;
import java.util.UUID;

public class Customer {
    UUID id;
    String lastName;
    String firstName;
    String middleName;
    LocalDate birthDay;
}
