package com.ub.nextcargo.domen;

import com.ub.nextcargo.Itinerary;
import com.ub.nextcargo.Location;
import com.ub.nextcargo.Voyage;

import java.time.LocalDateTime;

    final class Delivery {
    Voyage currentVoyage;
    Location lastKnownLocation;
    Enum transportationStatus;
    Enum routingStatus;
    LocalDateTime calculatedAt;

    Delivery(Enum routingStatus, Enum transportationStatus, LocalDateTime calculatedAt){
        this.routingStatus = routingStatus;
        this.transportationStatus = transportationStatus;
        this.calculatedAt = calculatedAt;
    };
}
