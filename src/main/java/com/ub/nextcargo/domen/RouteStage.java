package com.ub.nextcargo.domen;

import com.ub.nextcargo.Location;
import com.ub.nextcargo.Voyage;

import java.time.LocalDateTime;

public final class RouteStage {
    private Voyage voyage;
    private Location loadLocation;
    private Location unloadLocation;
    private LocalDateTime loadTime;
    private LocalDateTime unloadTime;
}
