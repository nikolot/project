package com.ub.nextcargo.domen;

import com.ub.nextcargo.Customer;
import com.ub.nextcargo.Location;

import javax.validation.constraints.Null;
import java.time.LocalDate;
import java.util.UUID;

public class Cargo {
    UUID id;
    Customer customer;
    Location origin;
    Location destination;
    LocalDate originDate;
    LocalDate destinationDate;
    double weight;
    Itinerary itinerary;
    Delivery delivery;

    Cargo(){};
    Cargo(Location origin, Location destination, LocalDate originDate, LocalDate destinationDate, double weight){
      this.origin = origin;
      this.destination = destination;
      this.originDate = originDate;
      this.destinationDate = destinationDate;
      this.weight = weight;
    };

    public void NewRoute(Location origin, Location destination, LocalDate originDate, LocalDate destinationDate){
        this.origin = origin;
        this.destination = destination;
        this.originDate = originDate;
        this.destinationDate = destinationDate;
    }

    public void NewItinerary(Itinerary itinerary, Enum routingStatus, Enum transportationStatus){
        this.itinerary = itinerary;
        this.delivery.routingStatus = routingStatus;
        this.delivery.transportationStatus = transportationStatus;
    }


}
