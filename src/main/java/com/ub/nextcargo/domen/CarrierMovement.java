package com.ub.nextcargo.domen;

import com.ub.nextcargo.Location;

import java.time.LocalDateTime;

public final class CarrierMovement {
    Location departuteLocation;
    Location arrivalLocation;
    LocalDateTime arrivalTime;
}
