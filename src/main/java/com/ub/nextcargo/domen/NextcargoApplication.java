package com.ub.nextcargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NextcargoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NextcargoApplication.class, args);
	}

}


